//vue.config.js
module.exports = {
	publicPath : '/betadmin/',
	transpileDependencies: ['vuetify'],
	devServer: {
		proxy: 'http://vseintegration.kironinteractive.com:8013'
	}
}